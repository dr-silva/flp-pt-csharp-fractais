﻿/*******************************************************************************
 *
 * Arquivo  : funcao-holomorfa.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa a função holomorfa e sua derivada, tem como responsa-
 *            bilidade encontrar a raiz dum ponto.
 *
 ******************************************************************************/
using System;
using Fractais.Expressoes;
using FractalLotus.Fundacao.Matematica;

namespace Fractais
{
  public class FuncaoHolomorfa
  {
    #region Atributos
    private readonly Expressao funcao,
                               derivada;
    #endregion

    #region Constructor
    public FuncaoHolomorfa(Expressao funcao, Expressao derivada)
    {
      this.funcao   = funcao   ?? throw new ArgumentNullException(nameof(funcao));
      this.derivada = derivada ?? throw new ArgumentNullException(nameof(derivada));
    }
    #endregion

    #region Interface
    public Complexo obterRaiz(Complexo x, out int iteracoes)
    {
      Complexo anterior;
      iteracoes = 0;

      do
      {
        if (Constantes.eTeto(iteracoes++))
          return Complexo.NaN;

        anterior   = x;
        Complexo f = funcao  .valorar(anterior),
                 d = derivada.valorar(anterior);

        if (Complexo.eNaN(f) || Complexo.eNaN(d) || Constantes.eZero(d))
          return Complexo.NaN;

        x = anterior - f / d;
      } while (!Constantes.saoIguais(anterior, x));

      return x;
    }

    public void imprimirInfos()
    {
      Console.WriteLine($"função  : {funcao}");
      Console.WriteLine($"derivada: {derivada}");
    }
    #endregion
  }
}
