﻿/*******************************************************************************
 *
 * Arquivo  : bacia-newton.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe responsável por desenhar o fractal.
 *
 ******************************************************************************/
using System;
using System.Drawing;
using Fractais.Expressoes;
using FractalLotus.Fundacao.Cerne;
using FractalLotus.Fundacao.Colecoes;
using FractalLotus.Fundacao.Matematica;
using FractalLotus.Fundacao.EntradasSaidas;

namespace Fractais
{
  public class BaciaNewton
  {
    #region Atributos
    private readonly Complexo inicio,
                              dimensao;
    private readonly string   arquivo;

    private readonly ImagemBitmap         fractal;
    private readonly FuncaoHolomorfa      funcao;
    private readonly IGeradorCongruencial aleatorio;
    private readonly BolsaEncadeada<ParRaizCor> raizes;
    private          Periodo              tempo;
    #endregion

    #region Interface
    public BaciaNewton(Config config)
    {
      arquivo   = config.arquivo;
      inicio    = new Complexo       (config.reInicial,  config.imInicial);
      dimensao  = new Complexo       (config.reDimensao, config.imDimensao);
      fractal   = new ImagemBitmap   (config.imgAltura,  config.imgLargura);
      funcao    = new FuncaoHolomorfa(config.funcao,     config.derivada);
      raizes    = new BolsaEncadeada<ParRaizCor>();
      aleatorio = FabricaGeradorCongruencial.criar
      (
        "posix", TipoGeradorCongruencial.Linear, config.semente
      );

      fractal.originarDoInferiorEsquerdo();
    }

    public void desenhar()
    {
      Temporizador temporizador = new Temporizador();

      for (int linha = 0; linha < fractal.altura; linha++)
        for (int coluna = 0; coluna < fractal.largura; coluna++)
        {
          Complexo x = calcularPonto(linha, coluna);
                   x = funcao.obterRaiz(x, out var iteracoes);

          fractal.pixel(linha, coluna, obterCor(x, iteracoes));
        }

      tempo = temporizador.decorrer();
    }

    public void salvar()
    {
      try
      {
        fractal.salvar(arquivo);

        // imprimir dados
        funcao.imprimirInfos();
        Console.WriteLine($"semente : {aleatorio.semente}");
        Console.WriteLine($"arquivo : {arquivo}");
        Console.WriteLine($"tempo   : {tempo}");
      }
      catch
      {
        // faça nada, não é para o programa cair neste catch
      }
    }
    #endregion

    #region Assistentes
    private Complexo calcularPonto(double linha, double coluna)
    {
      return new Complexo
      (
        inicio.re + dimensao.re * (coluna / (fractal.largura - 1)),
        inicio.im + dimensao.im * (linha  / (fractal.altura  - 1))
      );
    }

    private Color obterCor(Complexo raiz, int iteracoes)
    {
      if (Complexo.eNaN(raiz) || Constantes.eTeto(iteracoes))
        return Color.Black;

      Color cor = Color.Empty;

      foreach (var item in raizes)
        if (Constantes.saoIguais(raiz, item.raiz))
        {
          cor = item.cor;
          break;
        }

      if (cor == Color.Empty)
      {
        cor = Color.FromArgb(aleatorio.uniforme(0xFFFFFF));

        raizes.guardar(new ParRaizCor(raiz, cor));
      }

      double porcentagem = Constantes.porcentagem(iteracoes);
      return Color.FromArgb
      (
        (int)(cor.R * porcentagem),
        (int)(cor.G * porcentagem),
        (int)(cor.B * porcentagem)
      );
    }
    #endregion

    #region Raiz
    private class ParRaizCor
    {
      public readonly Complexo raiz;
      public readonly Color    cor;

      public ParRaizCor(Complexo raiz, Color cor)
      {
        this.raiz = raiz;
        this.cor  = cor;
      }
    }
    #endregion
  }
}
