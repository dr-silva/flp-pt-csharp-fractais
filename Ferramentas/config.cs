﻿/*******************************************************************************
 *
 * Arquivo  : config.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-14 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa os parâmetros de entrada do programa.
 *
 ******************************************************************************/
using System;
using System.IO;
using Fractais.Expressoes;
using FractalLotus.Fundacao.Colecoes;

namespace Fractais
{
  public class Config
  {
    #region Atributos
    private readonly BolsaEncadeada<string> fErros;
    #endregion

    #region Interface
    public Config()
    {
      fErros = new BolsaEncadeada<string>();

      reInicial  = double.NaN;
      imInicial  = double.NaN;
      reDimensao = double.NaN;
      imDimensao = double.NaN;
      imgAltura  = int.MinValue;
      imgLargura = int.MinValue;
      arquivo    = null;
      semente    = (uint)Environment.TickCount;
      funcao     = null;
      derivada   = null;
    }

    public void lerDados()
    {
      if (Console.IsInputRedirected)
        using (StreamReader leitor = new StreamReader(Console.OpenStandardInput()))
        {
          lerDados(leitor);
          leitor.Close();
          lerDerivada();
          validarLeitura();
        }
      else
      {
        fErros.guardar("A entrada do console não foi redirecionada.");
        fErros.guardar("Use a opção \"ajuda\" para mais informações.");
      }
    }

    public void imprimirErros()
    {
      foreach (string erro in fErros)
        Console.WriteLine(erro);
    }

    public static void imprimirArquivo()
    {
      if (Console.IsOutputRedirected)
        using (var escritor = new StreamWriter(Console.OpenStandardOutput()))
        {
          escritor.WriteLine("reInicial =0         # deve ser um ponto-flutuante");
          escritor.WriteLine("imInicial =0         # deve ser um ponto-flutuante");
          escritor.WriteLine("reDimensao=1         # deve ser um ponto-flutuante positivo");
          escritor.WriteLine("imDimensao=1         # deve ser um ponto-flutuante positivo");
          escritor.WriteLine("imgAltura =100       # deve ser um numero inteiro positivo");
          escritor.WriteLine("imgLargura=100       # deve ser um numero inteiro positivo");
          escritor.WriteLine("funcao    =x^4-1     # deve ser uma expressao que represente a funcao");
          escritor.WriteLine("#derivada  =4*x^3     # deve ser uma expressao que represente a derivada");
          escritor.WriteLine("#semente   =20211020  # deve ser um numero inteiro positivo");
          escritor.WriteLine("arquivo   =teste.png # deve ser arquivo cuja extensao e PNG");

          escritor.Close();
        }
      else
      {
        Console.WriteLine("A saída do console não foi redirecionada.");
        Console.WriteLine("Use a opção \"ajuda\" para mais informações.");
      }
    }
    #endregion

    #region Assistentes
    private void validarLeitura()
    {
      if (double.IsNaN(reInicial))
        fErros.guardar("A chave \"reInicial\" não consta no arquivo de configurações");
      if (double.IsNaN(imInicial))
        fErros.guardar("A chave \"imInicial\" não consta no arquivo de configurações");
      if (double.IsNaN(reDimensao))
        fErros.guardar("A chave \"reDimensao\" não consta no arquivo de configurações");
      if (double.IsNaN(imDimensao))
        fErros.guardar("A chave \"imDimensao\" não consta no arquivo de configurações");
      if (imgAltura == int.MinValue)
        fErros.guardar("A chave \"imgAltura\" não consta no arquivo de configurações");
      if (imgLargura == int.MinValue)
        fErros.guardar("A chave \"imgLargura\" não consta no arquivo de configurações");
      if (arquivo == null)
        fErros.guardar("A chave \"arquivo\" não consta no arquivo de configurações");
      if (funcao == null)
        fErros.guardar("A chave \"funcao\" não consta no arquivo de configurações");
    }

    private void lerDados(StreamReader leitor)
    {
      int intLinha = 1;

      while (!leitor.EndOfStream)
      {
        string strLinha = leitor.ReadLine();

        int i = strLinha.IndexOf('#');
        if (i >= 0)
          strLinha = strLinha.Substring(0, i).Trim();

        if (string.IsNullOrWhiteSpace(strLinha))
          continue;

        string[] par = strLinha.Split('=');

        if (par.Length != 2)
          fErros.guardar($"A linha {intLinha++} está fora do padrão chave=valor");
        else
          lerChaveValor
          (
            par[0].Trim().ToLowerInvariant(), par[1].Trim().ToLowerInvariant(), intLinha++
          );
      }
    }

    private void lerChaveValor(string chave, string valor, int linha)
    {
      switch (chave)
      {
        case "reinicial" : lerReInicial (valor, linha); break;
        case "iminicial" : lerImInicial (valor, linha); break;
        case "redimensao": lerReDimensao(valor, linha); break;
        case "imdimensao": lerImDimensao(valor, linha); break;
        case "imgaltura" : lerImgAltura (valor, linha); break;
        case "imglargura": lerImgLargura(valor, linha); break;
        case "semente"   : lerSemente   (valor, linha); break;
        case "arquivo"   : lerArquivo   (valor, linha); break;
        case "funcao"    : lerFuncao    (valor, linha); break;
        case "derivada"  : lerDerivada  (valor, linha); break;
        default          : fErros.guardar($"A chave \"{chave}\" na linha {linha} não é válida"); break;
      }
    }

    private void lerReInicial(string valor, int linha)
    {
      if (double.TryParse(valor, out var temp))
        reInicial = temp;
      else
        fErros.guardar($"O valor da chave \"reInicial\" (na linha {linha}) não é um número válido.");
    }

    private void lerImInicial(string valor, int linha)
    {
      if (double.TryParse(valor, out var temp))
        imInicial = temp;
      else
        fErros.guardar($"O valor da chave \"imInicial\" (na linha {linha}) não é um número válido.");
    }

    private void lerReDimensao(string valor, int linha)
    {
      if (double.TryParse(valor, out var temp))
      {
        if (temp > 0)
          reDimensao = temp;
        else
          fErros.guardar($"O valor da chave \"reDimensao\" (na linha {linha}) não é um número positivo válido.");
      }
      else
        fErros.guardar($"O valor da chave \"reDimensao\" (na linha {linha}) não é um número válido.");
    }

    private void lerImDimensao(string valor, int linha)
    {
      if (double.TryParse(valor, out var temp))
      {
        if (temp > 0)
          imDimensao = temp;
        else
          fErros.guardar($"O valor da chave \"imDimensao\" (na linha {linha}) não é um número positivo válido.");
      }
      else
        fErros.guardar($"O valor da chave \"imDimensao\" (na linha {linha}) não é um número válido.");
    }

    private void lerImgAltura(string valor, int linha)
    {
      if (int.TryParse(valor, out var temp))
      {
        if (temp > 0)
          imgAltura = temp;
        else
          fErros.guardar($"O valor da chave \"imgAltura\" (na linha {linha}) não é um número positivo válido.");
      }
      else
        fErros.guardar($"O valor da chave \"imgAltura\" (na linha {linha}) não é um número válido.");
    }

    private void lerImgLargura(string valor, int linha)
    {
      if (int.TryParse(valor, out var temp))
      {
        if (temp > 0)
          imgLargura = temp;
        else
          fErros.guardar($"O valor da chave \"imgLargura\" (na linha {linha}) não é um número positivo válido.");
      }
      else
        fErros.guardar($"O valor da chave \"imgLargura\" (na linha {linha}) não é um número válido.");
    }

    private void lerSemente(string valor, int linha)
    {
      if (uint.TryParse(valor, out var temp))
        semente = temp;
      else
        fErros.guardar($"O valor da chave \"semente\" (na linha {linha}) não é um número positivo válido.");
    }

    private void lerArquivo(string valor, int linha)
    {
      try
      {
        using (var teste = File.CreateText(valor))
        {
          teste.Close();
        }

        arquivo = valor;
      }
      catch
      {
        fErros.guardar($"O valor da chave \"arquivo\" (na linha {linha}) não é um arquivo válido.");
      }
    }

    private void lerFuncao(string valor, int linha)
    {
      try
      {
        funcao = Sintaxe.analisar(valor);
      }
      catch (ArgumentException)
      {
        fErros.guardar($"O valor da chave \"funcao\" (na linha {linha}) não é uma expressão válida.");
      }
    }

    private void lerDerivada(string valor, int linha)
    {
      try
      {
        derivada = Sintaxe.analisar(valor);
      }
      catch (ArgumentException)
      {
        fErros.guardar($"O valor da chave \"derivada\" (na linha {linha}) não é uma expressão válida.");
      }
    }

    private void lerDerivada()
    {
      if (funcao != null && derivada == null)
        try
        {
          derivada = Sintaxe.derivar(funcao);
        }
        catch (ArgumentException)
        {
          fErros.guardar($"A função \"{funcao}\" não é uma função holomorfa.");
        }
    }
    #endregion

    #region Propriedades
    public double    reInicial  { get; private set; }
    public double    imInicial  { get; private set; }
    public double    reDimensao { get; private set; }
    public double    imDimensao { get; private set; }
    public int       imgAltura  { get; private set; }
    public int       imgLargura { get; private set; }
    public uint      semente    { get; private set; }
    public string    arquivo    { get; private set; }
    public Expressao funcao     { get; private set; }
    public Expressao derivada   { get; private set; }

    public bool haErros
    {
      get { return !fErros.vazio; }
    }
    #endregion
  }
}
