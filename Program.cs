﻿/*******************************************************************************
 *
 * Arquivo  : Program.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-14 <yyyy-mm-dd>
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: ...
 *
 ******************************************************************************/
using System;

namespace Fractais
{
  class Program
  {
    static void Main(string[] args)
    {
      switch (args.Length)
      {
        case 0: executar();        break;
        case 1: executar(args[0]); break;
        default: imprimirErro();   break;
      }
    }

    private static void executar()
    {
      Config config = new Config();

      Console.WriteLine("Lendo os dados...");
      config.lerDados();

      if (config.haErros)
        config.imprimirErros();
      else
      {
        BaciaNewton bacia = new BaciaNewton(config);

        Console.WriteLine("Preparando o fractal...");
        bacia.desenhar();
        bacia.salvar();
      }
    }

    private static void executar(string opcao)
    {
      switch (opcao.Trim().ToLowerInvariant())
      {
        case "config": Config.imprimirArquivo(); break;
        case "ajuda" : imprimirAjuda();          break;
        default      : imprimirErro();           break;
      }
    }

    private static void imprimirAjuda()
    {
      Console.WriteLine("use fractais [ajuda] [config > config.txt] [< config.txt]");
      Console.WriteLine();
      Console.WriteLine("    ajuda                 imprimi as informações de uso");
      Console.WriteLine("    config > config.txt   gera o arquivo de configuração");
      Console.WriteLine("    < config.txt          gera o fractal com base no arquivo de configuração");
    }

    private static void imprimirErro()
    {
      Console.WriteLine("Opções de entrada desconhecidos.");
      Console.WriteLine("Use a opção \"ajuda\" para mais informações.");
    }
  }
}
