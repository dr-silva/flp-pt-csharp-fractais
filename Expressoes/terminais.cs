﻿/*******************************************************************************
 *
 * Arquivo  : terminais.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Contém os representantes dos nós terminais da expressão conforme
 *            documentação
 *
 ******************************************************************************/
using System;
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  class Variavel : Expressao
  {
    #region Expressão
    public override Complexo valorar(Complexo x)
    {
      return x;
    }

    public override string ToString()
    {
      return "x";
    }

    protected internal override Expressao clonar()
    {
      return new Variavel();
    }

    protected internal override Expressao derivar()
    {
      return new Numero(new Complexo(1, 0));
    }

    protected internal override Expressao simplificar()
    {
      return this;
    }
    #endregion
  }

  class Numero : Expressao
  {
    #region Número
    private readonly Complexo valor;

    public Numero(Complexo valor)
    {
      this.valor = valor;
    }

    public bool eZero()
    {
      return Constantes.eZero(valor);
    }

    public bool eNaN()
    {
      return Complexo.eNaN(valor);
    }

    public bool eUm()
    {
      return Constantes.saoIguais(valor, new Complexo(1, 0));
    }

    public bool eConstEuler()
    {
      return Constantes.saoIguais(valor, new Complexo(Math.E, 0));
    }

    public bool eReal()
    {
      return Constantes.eReal(valor);
    }

    public bool eImaginario()
    {
      return Constantes.eImaginario(valor);
    }
    #endregion

    #region Expressão
    public override Complexo valorar(Complexo x)
    {
      return valor;
    }

    public override string ToString()
    {
      return $"({valor})";
    }

    protected internal override Expressao clonar()
    {
      return new Numero(valor);
    }

    protected internal override Expressao derivar()
    {
      return new Numero(Complexo.ZERO);
    }

    protected internal override Expressao simplificar()
    {
      return this;
    }
    #endregion
  }
}
