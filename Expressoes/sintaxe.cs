﻿/*******************************************************************************
 *
 * Arquivo  : sintaxe.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: ...
 *
 ******************************************************************************/
using System;
using FractalLotus.Fundacao.Colecoes;

namespace Fractais.Expressoes
{
  public class Sintaxe : IDisposable
  {
    #region Atributos
    private readonly string frase;
    private readonly PilhaEncadeada<string>    operadores;
    private readonly PilhaEncadeada<Expressao> termos;
    #endregion

    #region Construtor & Destrutor
    public Sintaxe(string frase)
    {
      if (string.IsNullOrWhiteSpace(frase))
        throw new ArgumentNullException(nameof(frase));

      this.frase = frase;
      operadores = new PilhaEncadeada<string>();
      termos     = new PilhaEncadeada<Expressao>();
    }

    public void Dispose()
    {
    }
    #endregion

    #region Assistentes
    private Expressao analisar()
    {
      char operador = '\0';
      int  inicio   = 0;

      do
      {
        int    fim   = proximo(inicio);
        string termo = frase.Substring(inicio, fim - inicio).Trim();

        if (!string.IsNullOrWhiteSpace(termo))
          empilhar(termo);
        if (fim == frase.Length)
          break;

        operador = this.operador(frase[fim], operador, termo);

        if (operador != '(')
          desempilhar(string.Empty + operador);
        if (operador != ')')
          operadores.empilhar(string.Empty + operador);

        inicio = fim + 1;
      } while (inicio < frase.Length);

      return (operadores.total != 0 || termos.total != 1) ? null : termos.desempilhar();
    }

    private int proximo(int inicio)
    {
      for (int i = inicio; i < frase.Length; i++)
        if (Fabrica.eOperador(frase[i]))
          return i;

      return frase.Length;
    }

    private char operador(char atual, char anterior, string termo)
    {
      if (atual != '-' && atual != '+')
        return atual;
      else if (anterior == '(' && termo == string.Empty)
        return (atual == '-') ? '!' : '?';
      else
        return atual;
    }

    private void empilhar(string termo)
    {
      if (Fabrica.eFuncao(termo))
        operadores.empilhar(termo);
      else if (Fabrica.eNumero(termo))
        termos.empilhar( Fabrica.criarNumero(termo) );
      else if (Fabrica.eVariavel(termo))
        termos.empilhar( Fabrica.criarVariavel() );
      else
        throw new ArgumentOutOfRangeException(nameof(termo));
    }

    private void desempilhar(string ate)
    {
      while (!operadores.vazio && comparar(ate, operadores.espiar()) <= 0)
      {
        string    operador = operadores.desempilhar();
        Expressao direita  = termos    .desempilhar();

        if (Fabrica.eFuncao(operador))
          termos.empilhar( Fabrica.criarFuncao(operador, direita) );
        else if (operador == "(")
        {
          termos.empilhar(direita);   // devolve o que retirou
          break;
        }
        else
        {
          Expressao esquerda = null;

          if (Fabrica.eOperador(operador[0]))
            esquerda = termos.desempilhar();

          termos.empilhar( Fabrica.criarOperador(operador[0], esquerda, direita) );
        }
      }
    }

    private static int comparar(string esquerda, string direita)
    {
      return prioridade(esquerda) - prioridade(direita);
    }

    private static int prioridade(string operador)
    {
      if (Fabrica.eFuncao(operador))
        return 3;
      else
        switch (operador)
        {
          case "+":
          case "-": return 1;
          case "*":
          case "/": return 2;
          case "^": return 3;
          case "?":
          case "!": return 4;
          default : return 0;
        }
    }
    #endregion

    #region Interface
    public static Expressao analisar(string frase)
    {
      frase = string.Format($"({frase.ToLowerInvariant()})");

      using (Sintaxe sintaxe = new Sintaxe(frase))
      {
        try
        {
          Expressao result = sintaxe.analisar(),
                    anterior;

          if (result == null)
            throw new ArgumentException("frase inválida");

          do
          {
            anterior = result;
            result   = result.simplificar();
          } while (anterior != result);

          return result;
        }
        catch
        {
          throw new ArgumentException("frase inválida");
        }
      }
    }

    public static Expressao derivar(Expressao expressao)
    {
      if (expressao == null)
        throw new ArgumentException("expressão inválida");

      Expressao result = expressao.derivar(),
                anterior;

      do
      {
        anterior = result;
        result   = result.simplificar();
      } while (anterior != result);

      if (result is Numero)
      {
        Numero numero = (Numero)result;

        if (numero.eZero() || numero.eNaN())
          throw new ArgumentException("expressão inválida");
      }

      return result;
    }
    #endregion
  }
}
