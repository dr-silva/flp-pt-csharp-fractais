﻿/*******************************************************************************
 *
 * Arquivo  : expresao.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representa o nó da árvore sintática das expressões matemáticas.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  public abstract class Expressao
  {
    public abstract Complexo valorar(Complexo x);

    protected internal abstract Expressao clonar();
    protected internal abstract Expressao derivar();
    protected internal abstract Expressao simplificar();
  }
}
