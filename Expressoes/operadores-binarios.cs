﻿/*******************************************************************************
 *
 * Arquivo  : operadores-binarios.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representantes das operações sobre dois operandos, um à esquerda
 *            e outro à direita do operador.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  abstract class OperadorBinario : Expressao
  {
    #region Atributos
    private char      operador;
    private Expressao esquerda,
                      direita;
    #endregion

    #region Construtor
    public OperadorBinario(char operador, Expressao esquerda, Expressao direita)
    {
      this.operador = operador;
      this.esquerda = esquerda;
      this.direita  = direita;
    }
    #endregion

    #region Assistentes
    protected abstract Complexo  calcular   (Complexo  esquerda, Complexo  direita);
    protected abstract Expressao simplificar(Expressao esquerda, Expressao direita);
    protected abstract Expressao derivar    (Expressao esquerda, Expressao direita);
    protected abstract Expressao clonar     (Expressao esquerda, Expressao direita);
    #endregion

    #region Expressão
    public override Complexo valorar(Complexo x)
    {
      return calcular(esquerda.valorar(x), direita.valorar(x));
    }

    public override string ToString()
    {
      return $"({esquerda} {operador} {direita})";
    }

    protected internal override Expressao simplificar()
    {
      direita  = direita .simplificar();
      esquerda = esquerda.simplificar();

      if (esquerda is Numero && direita is Numero)
        return new Numero
        (
          calcular(esquerda.valorar(Complexo.ZERO), direita.valorar(Complexo.ZERO))
        );
      else if (esquerda is Numero || direita is Numero)
        return simplificar(esquerda, direita);
      else
        return this;
    }

    protected internal override Expressao derivar()
    {
      return derivar(esquerda.clonar(), direita.clonar());
    }

    protected internal override Expressao clonar()
    {
      return clonar(esquerda.clonar(), direita.clonar());
    }
    #endregion
  }

  class OperadorAdicao : OperadorBinario
  {
    public OperadorAdicao(Expressao esquerda, Expressao direita) : base('+', esquerda, direita)
    {
    }

    protected override Complexo  calcular(Complexo esquerda, Complexo direita)
    {
      return esquerda + direita;
    }

    protected override Expressao simplificar(Expressao esquerda, Expressao direita)
    {
      if (esquerda is Numero && ((Numero)esquerda).eZero())
        return direita;
      else if (direita is Numero && ((Numero)direita).eZero())
        return esquerda;
      else
        return this;
    }

    protected override Expressao derivar(Expressao esquerda, Expressao direita)
    {
      return new OperadorAdicao(esquerda.derivar(), direita.derivar());
    }

    protected override Expressao clonar(Expressao esquerda, Expressao direita)
    {
      return new OperadorAdicao(esquerda, direita);
    }
  }

  class OperadorSubtracao : OperadorBinario
  {
    public OperadorSubtracao(Expressao esquerda, Expressao direita) : base('-', esquerda, direita)
    {
    }

    protected override Complexo  calcular(Complexo esquerda, Complexo direita)
    {
      return esquerda - direita;
    }

    protected override Expressao simplificar(Expressao esquerda, Expressao direita)
    {
      if (direita is Numero && ((Numero)direita).eZero())
        return esquerda;
      else if (esquerda is Numero && ((Numero)esquerda).eZero())
        return (new OperadorNegativo(direita)).simplificar();
      else
        return this;
    }

    protected override Expressao derivar(Expressao esquerda, Expressao direita)
    {
      return new OperadorSubtracao(esquerda.derivar(), direita.derivar());
    }

    protected override Expressao clonar(Expressao esquerda, Expressao direita)
    {
      return new OperadorSubtracao(esquerda, direita);
    }
  }

  class OperadorMultiplicacao : OperadorBinario
  {
    public OperadorMultiplicacao(Expressao esquerda, Expressao direita) : base('*', esquerda, direita)
    {
    }

    protected override Complexo calcular(Complexo esquerda, Complexo direita)
    {
      return esquerda * direita;
    }

    protected override Expressao simplificar(Expressao esquerda, Expressao direita)
    {
      bool      eEsquerda = esquerda is Numero;
      Numero    numero    = eEsquerda ? (Numero)esquerda : (Numero)direita;
      Expressao expressao = eEsquerda ? direita : esquerda;

      if (numero.eZero())
        return new Numero(Complexo.ZERO);
      else if (numero.eUm())
        return expressao;
      else
        return this;
    }

    protected override Expressao derivar(Expressao esquerda, Expressao direita)
    {
      Expressao diffEsq = esquerda.derivar(),
                diffDir = direita .derivar(),
                novaEsq = new OperadorMultiplicacao(diffEsq,  direita),
                novaDir = new OperadorMultiplicacao(esquerda, diffDir);

      return new OperadorAdicao(novaEsq, novaDir).simplificar();
    }

    protected override Expressao clonar(Expressao esquerda, Expressao direita)
    {
      return new OperadorMultiplicacao(esquerda, direita);
    }
  }

  class OperadorDivisao : OperadorBinario
  {
    public OperadorDivisao(Expressao esquerda, Expressao direita) : base('/', esquerda, direita)
    {
    }

    protected override Complexo calcular(Complexo esquerda, Complexo direita)
    {
      return esquerda / direita;
    }

    protected override Expressao simplificar(Expressao esquerda, Expressao direita)
    {
      bool   eEsquerda = esquerda is Numero;
      Numero numero    = eEsquerda ? (Numero)esquerda : (Numero)direita;

      if (numero.eZero())
        return new Numero(eEsquerda ? Complexo.ZERO : Complexo.NaN);
      else if (numero.eUm())
        return eEsquerda ? this : esquerda;
      else
        return this;
    }

    protected override Expressao derivar(Expressao esquerda, Expressao direita)
    {
      Expressao diffEsq     = esquerda.derivar(),
                diffDir     = direita .derivar(),
                novaEsq     = new OperadorMultiplicacao(diffEsq,  direita),
                novaDir     = new OperadorMultiplicacao(esquerda, diffDir),
                numerador   = new OperadorSubtracao(novaEsq, novaDir),
                potencia    = new Numero(new Complexo(2, 0)),
                denominador = new OperadorExponenciacao(direita, potencia),
                result      = new OperadorDivisao(numerador, denominador);

      return result.simplificar();
    }

    protected override Expressao clonar(Expressao esquerda, Expressao direita)
    {
      return new OperadorDivisao(esquerda, direita);
    }
  }

  class OperadorExponenciacao : OperadorBinario
  {
    public OperadorExponenciacao(Expressao esquerda, Expressao direita) : base('^', esquerda, direita)
    {
    }

    protected override Complexo calcular(Complexo esquerda, Complexo direita)
    {
      return Complexo.exp(esquerda, direita);
    }

    protected override Expressao simplificar(Expressao esquerda, Expressao direita)
    {
      return direita is Numero && ((Numero)direita).eUm() ? esquerda : this;
    }

    protected override Expressao derivar(Expressao esquerda, Expressao direita)
    {
      Expressao result;

      if (esquerda is Numero)
      {
        Numero    numero  = (Numero)esquerda;
        Expressao novaEsq = new OperadorExponenciacao(esquerda, direita);

        result = new OperadorMultiplicacao(novaEsq, direita.derivar());

        if (!numero.eConstEuler())
        {
          Expressao novaDir = new LogaritmoNatural(esquerda.clonar());

          result = new OperadorMultiplicacao(result, novaDir);
        }
      }
      else
      {
        Expressao expoente = new OperadorSubtracao(direita, new Numero(new Complexo(1, 0))),
                  funcao   = new OperadorExponenciacao(esquerda, expoente),
                  novaEsq  = new OperadorMultiplicacao(funcao, direita.clonar()),
                  novaDir  = esquerda.derivar();

        result = new OperadorMultiplicacao(novaEsq, novaDir);
      }

      return result.simplificar();
    }

    protected override Expressao clonar(Expressao esquerda, Expressao direita)
    {
      return new OperadorExponenciacao(esquerda, direita);
    }
  }
}
