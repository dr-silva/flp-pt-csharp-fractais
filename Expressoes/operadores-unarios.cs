﻿/*******************************************************************************
 *
 * Arquivo  : operadores-unarios.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Contém os representantes das operações sobre um único operando.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  abstract class OperadorUnario : Expressao
  {
    #region Atributos
    private char      operador;
    private Expressao operando;
    #endregion

    #region Construtor
    public OperadorUnario(char operador, Expressao operando)
    {
      this.operador = operador;
      this.operando = operando;
    }
    #endregion

    #region Assistentes
    protected abstract Complexo  calcular   (Complexo  operando);
    protected abstract Expressao simplificar(Expressao operando);
    protected abstract Expressao clonar     (Expressao operando);
    #endregion

    #region Expressão
    public override Complexo valorar(Complexo x)
    {
      return calcular(operando.valorar(x));
    }

    public override string ToString()
    {
      return $"({operador}{operando})";
    }

    protected internal override Expressao simplificar()
    {
      operando = operando.simplificar();

      if (operando is Numero)
        return new Numero(calcular(operando.valorar(Complexo.ZERO)));
      else if (operando.GetType() == this.GetType())
        return ((OperadorUnario)operando).operando;
      else
        return simplificar(operando);
    }

    protected internal override Expressao derivar()
    {
      return clonar(operando.derivar());
    }

    protected internal override Expressao clonar()
    {
      return clonar(operando.clonar());
    }
    #endregion
  }

  class OperadorPositivo : OperadorUnario
  {
    public OperadorPositivo(Expressao operando) : base('+', operando)
    {
    }

    protected override Complexo calcular(Complexo operando)
    {
      return +operando;
    }

    protected override Expressao simplificar(Expressao operando)
    {
      return operando;
    }

    protected override Expressao clonar(Expressao operando)
    {
      return new OperadorPositivo(operando);
    }
  }

  class OperadorNegativo : OperadorUnario
  {
    public OperadorNegativo(Expressao operando) : base('-', operando)
    {
    }

    protected override Complexo calcular(Complexo operando)
    {
      return -operando;
    }

    protected override Expressao simplificar(Expressao operando)
    {
      return this;
    }

    protected override Expressao clonar(Expressao operando)
    {
      return new OperadorNegativo(operando);
    }
  }
}
