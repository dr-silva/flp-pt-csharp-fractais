﻿/*******************************************************************************
 *
 * Arquivo  : fabrica.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: ...
 *
 ******************************************************************************/
using System;
using System.Text.RegularExpressions;
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  static class Fabrica
  {
    #region Assistentes
    public static bool matches(this string value, string pattern)
    {
      return (new Regex(pattern)).IsMatch(value);
    }
    #endregion

    #region Perguntas
    public static bool eFuncao(string funcao)
    {
      return funcao.matches("^((sen|cos)h?|ln)$");
    }

    public static bool eOperador(char operador)
    {
      return (operador == '-') ||
             (operador == '+') ||
             (operador == '*') ||
             (operador == '/') ||
             (operador == '^') ||
             (operador == '(') ||
             (operador == ')');
    }

    public static bool eNumero(string numero)
    {
      return numero.matches("^((e|pi|\\d+(\\.\\d+)?|\\.\\d+)i?|i)$");
    }

    public static bool eVariavel(string termo)
    {
      return (termo == "x");
    }
    #endregion

    #region Instanciações
    public static Expressao criarFuncao(string funcao, Expressao entrada)
    {
      switch (funcao)
      {
        case "sen" : return new SenoTrigonometrico   (entrada);;
        case "cos" : return new CossenoTrigonometrico(entrada);
        case "senh": return new SenoHiperbolico      (entrada);
        case "cosh": return new CossenoHiperbolico   (entrada);
        case "ln"  : return new LogaritmoNatural     (entrada);
        default    : return null;
      }
    }

    public static Expressao criarOperador(char operador, Expressao esquerda, Expressao direita)
    {
      switch (operador)
      {
        case '!': return new OperadorNegativo     (direita);
        case '?': return new OperadorPositivo     (direita);
        case '+': return new OperadorAdicao       (esquerda, direita);
        case '-': return new OperadorSubtracao    (esquerda, direita);
        case '*': return new OperadorMultiplicacao(esquerda, direita);
        case '/': return new OperadorDivisao      (esquerda, direita);
        case '^': return new OperadorExponenciacao(esquerda, direita);
        default : return null;
      }
    }

    public static Expressao criarNumero(string valor)
    {
      Complexo numero;

      switch (valor)
      {
        case "i"  : numero = new Complexo(0, 1);       break;
        case "e"  : numero = new Complexo(Math.E, 0);  break;
        case "ei" : numero = new Complexo(0, Math.E);  break;
        case "pi" : numero = new Complexo(Math.PI, 0); break;
        case "pii": numero = new Complexo(0, Math.PI); break;
        default:
          bool   eImaginario = valor.Contains("i");
          double neoValor    = double.Parse
          (
            eImaginario ? valor.Substring(0, valor.Length - 1) : valor
          );

          numero = eImaginario ? new Complexo(0, neoValor) : new Complexo(neoValor, 0);

          break;
      }

      return new Numero(numero);
    }

    public static Expressao criarVariavel()
    {
      return new Variavel();
    }
    #endregion
  }
}
