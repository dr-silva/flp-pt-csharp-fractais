﻿/*******************************************************************************
 *
 * Arquivo  : constantes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-16
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Classe de utilidades para o sistema.
 *
 ******************************************************************************/
using System;
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  public static class Constantes
  {
    private const double EPSILON   = 1e-9;
    private const int    ITERACOES = 40;

    public static bool saoIguais(Complexo esquerda, Complexo direita)
    {
      return (esquerda - direita).modulo < EPSILON;
    }

    public static bool eZero(Complexo valor)
    {
      return Complexo.eZero(valor, EPSILON);
    }

    public static bool eReal(Complexo valor)
    {
      return Math.Abs(valor.im) < EPSILON;
    }

    public static bool eImaginario(Complexo valor)
    {
      return Math.Abs(valor.re) < EPSILON;
    }

    public static bool eTeto(int iteracoes)
    {
      return iteracoes > ITERACOES;
    }

    public static double porcentagem(int iteracoes)
    {
      iteracoes = Math.Min(iteracoes, ITERACOES);

      return 1.0 - (iteracoes / (double)ITERACOES);
    }
  }
}
