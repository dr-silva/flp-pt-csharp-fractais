﻿/*******************************************************************************
 *
 * Arquivo  : funcoes.cs
 * Autor    : Daniel Raimundo da Silva
 * Data     : 2021-10-13
 * Licença  : Este arquivo está sob licença Non-Comercial Creative Commons
 * Descrição: Representam as funções nas expressões matemáticas.
 *
 ******************************************************************************/
using FractalLotus.Fundacao.Matematica;

namespace Fractais.Expressoes
{
  abstract class Funcao : Expressao
  {
    #region Atributos
    private string    nome;
    private Expressao entrada;
    #endregion

    #region Construtor
    public Funcao(string nome, Expressao entrada)
    {
      this.nome    = nome;
      this.entrada = entrada;;
    }
    #endregion

    #region Assistentes
    protected abstract Complexo  calcular(Complexo  entrada);
    protected abstract Expressao derivar (Expressao entrada);
    protected abstract Expressao clonar  (Expressao entrada);
    #endregion

    #region Expressão
    public override Complexo valorar(Complexo x)
    {
      return calcular(entrada.valorar(x));
    }

    public override string ToString()
    {
      return $"{nome}({entrada})";
    }

    protected internal override Expressao simplificar()
    {
      entrada = entrada.simplificar();

      if (entrada is Numero)
        return new Numero(calcular(entrada.valorar(Complexo.ZERO)));
      else
        return this;
    }

    protected internal override Expressao derivar()
    {
      Expressao derivada = derivar(entrada.clonar()),
                result;

      if (entrada is Variavel)
        result = derivada;
      else
        result = new OperadorMultiplicacao(derivada, entrada.derivar());

      return result.simplificar();
    }

    protected internal override Expressao clonar()
    {
      return clonar(entrada.clonar());
    }
    #endregion
  }

  class SenoTrigonometrico : Funcao
  {
    public SenoTrigonometrico(Expressao entrada) : base("sen", entrada)
    {
    }

    protected override Complexo  calcular(Complexo entrada)
    {
      return Complexo.sen(entrada);
    }

    protected override Expressao derivar(Expressao entrada)
    {
      return new CossenoTrigonometrico(entrada);
    }

    protected override Expressao clonar(Expressao entrada)
    {
      return new SenoTrigonometrico(entrada);
    }
  }

  class CossenoTrigonometrico : Funcao
  {
    public CossenoTrigonometrico(Expressao entrada) : base("cos", entrada)
    {
    }

    protected override Complexo  calcular(Complexo entrada)
    {
      return Complexo.cos(entrada);
    }

    protected override Expressao derivar(Expressao entrada)
    {
      return new OperadorNegativo(new SenoTrigonometrico(entrada));
    }

    protected override Expressao clonar(Expressao entrada)
    {
      return new CossenoTrigonometrico(entrada);
    }
  }

  class SenoHiperbolico : Funcao
  {
    public SenoHiperbolico(Expressao entrada) : base("senh", entrada)
    {
    }

    protected override Complexo  calcular(Complexo entrada)
    {
      return Complexo.senh(entrada);
    }

    protected override Expressao derivar(Expressao entrada)
    {
      return new CossenoHiperbolico(entrada);
    }

    protected override Expressao clonar(Expressao entrada)
    {
      return new SenoHiperbolico(entrada);
    }
  }

  class CossenoHiperbolico : Funcao
  {
    public CossenoHiperbolico(Expressao entrada) : base("cosh", entrada)
    {
    }

    protected override Complexo  calcular(Complexo entrada)
    {
      return Complexo.cosh(entrada);
    }

    protected override Expressao derivar(Expressao entrada)
    {
      return new SenoHiperbolico(entrada);
    }

    protected override Expressao clonar(Expressao entrada)
    {
      return new CossenoHiperbolico(entrada);
    }
  }

  class LogaritmoNatural : Funcao
  {
    public LogaritmoNatural(Expressao entrada) : base("ln", entrada)
    {
    }

    protected override Complexo  calcular(Complexo entrada)
    {
      return Complexo.ln(entrada);
    }

    protected override Expressao derivar(Expressao entrada)
    {
      Expressao numerador = new Numero(new Complexo(1, 0));

      return new OperadorDivisao(numerador, entrada);
    }

    protected override Expressao clonar(Expressao entrada)
    {
      return new LogaritmoNatural(entrada);
    }
  }
}
